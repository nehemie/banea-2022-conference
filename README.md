---
filename: README
author: Néhémie
date: 14 December 2021
---

# BANEA Conference

## Call for paper

> British Association for the Archaeology of the Ancient Near
> East (BANEA) 2022 Conference
>
> Cambridge University, 7–8 January, 2022
>
> The BANEA conference for 2022 will be hosted by the University
> of Cambridge, and the Call for Papers is open. Due to the
> ongoing Covid19 epidemic, the conference will be held
> virtually.
>
> The conference themes are:
> 1) Rethinking the urban revolution
> 2) Climate change
> 3) Text-aided archaeology
>
> We are also open to proposals for papers or panels on other
> topics relevant to the archaeology, history and texts of the
> Ancient Near East, including reports on recent fieldwork,
> archive or museum work, and public engagement. We are
> particularly interested in papers that address the many
> challenging issues of decolonisation.
>
> Please send proposals (title and abstract of 200 words maximum)
> to the organisers, Augusta McMahon (amm36@cam.ac.uk) and/or
> Cameron Petrie (cap59@cam.ac.uk) by 15 December. Please
> indicate your time zone, to assist us with scheduling.
>
> There is no charge to attend the conference, although we ask
> that all delegates and paper presenters join BANEA as members.
> Information on membership can be found on the BANEA website:
> http://banealcane.org/banea/
>
> Details on how to register will be advertised soon and will be
> linked through the BANEA website.

## Organisation

    .
    ├── abstract
    │   ├── abstract.md
    │   └── Makefile
    └── README.md

