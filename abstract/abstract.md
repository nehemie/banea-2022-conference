## Title: "Testing our Knowledge. Sites Density Trends on the Anatolian Plateau during the Bronze Age".

## Author: Néhémie STRUPLER  (ns817@cam.ac.uk)

## Affiliation:
 - Visiting Fellow, The McDonald Institute for Archaeological Research, University of Cambridge


## Abstract

Knowledge about the past is dramatically uneven. We know
incredible precise facts. We have countless records about the
amount and the quality of silver exchanged against a certain type
of textile, who performs the transaction, where and when it
happens. If excavated not too long ago, we have any artefact’s
exact geolocation, date of discovery, stratum description,
radiocarbon date, physical or chemical analyses. However, we
badly perform at knowing what we have discovered globally. A
sample of answers to the basic and crude questions about how many
Bronze Age sites have already been recorded, and what are the
main trends of their evolution would probably demonstrate a low
degree of agreement. An average of 3’141 thousand ± 10’000 sites
(95% confidence interval)?

In Central Anatolia, the earliest urban societies emerged during
the Bronze Age (3000-1000 BCE). Over only two thousand years,
cities appear and are first organized in competing polities,
so-called city-states. After episodes of endemic warfare, these
cities are reorganized at the end of this period into a
territorial state, the Hittite Empire. My talk will show how a
probabilistic multivariate and multiscale approach addresses the
problem of temporal uncertainty and the problem of fuzzy dating.
It aims to formalize a better understanding of how site densities
are changing on the Anatolian Plateau during the Anatolian Bronze
Age.

